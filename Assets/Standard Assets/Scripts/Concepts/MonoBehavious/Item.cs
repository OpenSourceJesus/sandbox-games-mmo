using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mirandus
{
	public class Item : MonoBehaviour
	{
		public virtual void Equip ()
		{
			gameObject.SetActive(true);
		}

		public virtual void Unequip ()
		{
			gameObject.SetActive(false);
		}
	}
}