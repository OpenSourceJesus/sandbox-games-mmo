using Mirandus;
using Extensions;
using UnityEngine;

public class SpatialHashGrid2DTest : UpdateWhileEnabled
{
	public Rect rect;
	public float cellSize;
	public Patrol[] patrols = new Patrol[0];
	public SpatialHashGrid2D<Patrol> spatialHashGrid = new SpatialHashGrid2D<Patrol>();
	public SpatialHashGrid2D<Patrol>.Agent[] agents = new SpatialHashGrid2D<Patrol>.Agent[0];

	public override void OnEnable ()
	{
		spatialHashGrid = new SpatialHashGrid2D<Patrol>(rect, cellSize);
		if (patrols.Length == 0)
			patrols = FindObjectsOfType<Patrol>();
		agents = new SpatialHashGrid2D<Patrol>.Agent[patrols.Length];
		for (int i = 0; i < patrols.Length; i ++)
		{
			Patrol patrol = patrols[i];
			SpatialHashGrid2D<Patrol>.Agent agent = new SpatialHashGrid2D<Patrol>.Agent(patrol.trs.position.GetXZ(), patrol, spatialHashGrid);
			agents[i] = agent;
		}
		base.OnEnable ();
	}

	public void DoUpdate ()
	{
		for (int i = 0; i < agents.Length; i ++)
		{
			SpatialHashGrid2D<Patrol>.Agent agent = agents[i];
			agent.value.DoUpdate ();
			agent.Update (agent.value.trs.position.GetXZ());
		}
	}
}