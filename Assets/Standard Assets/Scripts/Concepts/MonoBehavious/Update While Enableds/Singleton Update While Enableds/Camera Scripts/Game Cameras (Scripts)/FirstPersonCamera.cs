using UnityEngine;
using Extensions;
using UnityEngine.InputSystem;

namespace Mirandus
{
	public class FirstPersonCamera : GameCamera
	{
		public float rotateRate;

		public override void HandleRotation ()
		{
			Vector2 rotaInput = Mouse.current.delta.ReadValue().FlipY() * rotateRate * Time.unscaledDeltaTime;
			trs.Rotate(trs.right * rotaInput.y);
			Player.instance.trs.Rotate(Vector3.up * rotaInput.x);
		}
	}
}