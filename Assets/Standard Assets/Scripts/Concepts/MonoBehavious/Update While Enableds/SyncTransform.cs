using UnityEngine;
using Extensions;
using DarkRift;
using DarkRift.Client;
using DarkRift.Client.Unity;
using TMPro;
using System.Collections.Generic;

namespace Mirandus
{
	public class SyncTransform : UpdateWhileEnabled
	{
		public Transform trs;
		public uint id;
		public float minDistanceToSyncPosition;
		public float minAngleToSyncYRotation;
		public TMP_Text idText;
		public RectTransform canvasTrs;
		float minDistanceToSyncPositionSqr;
		Vector3 lastSyncedPosition;
		float lastSyncedYRotation;

		void Awake ()
		{
			idText.text = "" + id;
		}

		public override void OnEnable ()
		{
			base.OnEnable ();
			minDistanceToSyncPositionSqr = minDistanceToSyncPosition * minDistanceToSyncPosition;
			// NetworkManager.syncTransformsDict.Add(id, this);
		}
		
		public override void DoUpdate ()
		{
			if ((trs.position - lastSyncedPosition).sqrMagnitude >= minDistanceToSyncPositionSqr)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(id);
					writer.Write(trs.position.x);
					writer.Write(trs.position.y);
					writer.Write(trs.position.z);
					using (Message message = Message.Create(NetworkManager.MessageTags.SYNC_TRANSFORM_MOVED, writer))
						NetworkManager.instance.client.SendMessage(message, SendMode.Unreliable);
				}
				lastSyncedPosition = trs.position;
			}
			if (Mathf.DeltaAngle(trs.eulerAngles.y, lastSyncedYRotation) >= minAngleToSyncYRotation)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(id);
					writer.Write(trs.eulerAngles.y);
					using (Message message = Message.Create(NetworkManager.MessageTags.SYNC_TRANSFORM_ROTATED, writer))
						NetworkManager.instance.client.SendMessage(message, SendMode.Unreliable);
				}
				lastSyncedYRotation = trs.eulerAngles.y;
			}
		}

		// public override void OnDisable ()
		// {
		// 	base.OnDisable ();
		// 	NetworkManager.syncTransformsDict.Remove(id);
		// }
	}
}