using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Mirandus
{
	public class NotificationText : Spawnable
	{
        public TMP_Text text;
	}
}