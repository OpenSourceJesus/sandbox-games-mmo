using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mirandus
{
	public class UsableItem : Item
	{
		public int usedCount;
		public bool limitUses;
		public int maxUseCount;

		public virtual bool TryUse ()
		{
			if (!limitUses || usedCount < maxUseCount)
			{
				Use ();
				return true;
			}
			return false;
		}

		public virtual void Use ()
		{
			usedCount ++;
		}
	}
}