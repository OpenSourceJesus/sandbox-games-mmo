using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mirandus
{
	public class Sword : UsableItem
	{
		public Animator anim;
		public string attackAnimStateName;

		public override void Use ()
		{
			base.Use ();
			anim.Play (attackAnimStateName);
		}
	}
}