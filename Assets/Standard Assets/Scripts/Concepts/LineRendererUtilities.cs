using UnityEngine;
using Extensions;
using Mirandus;

public static class LineRendererUtilities
{
	public static void SetLineRenderersToBoundsSides (Bounds bounds, LineRenderer[] lineRenderers)
	{
		LineSegment3D[] sides = bounds.GetSides();
		for (int i = 0; i < 12; i ++)
		{
			LineSegment3D side = sides[i];
			lineRenderers[i].SetPositions(new Vector3[2] { side.start, side.end });
		}
	}

	public static LineRenderer AddLineRendererToGameObjectOrMakeNew (GameObject go)
	{
		if (go == null)
			go = new GameObject();
		else if (go.GetComponent<LineRenderer>() != null)
		{
			Transform trs = go.GetComponent<Transform>();
			go = new GameObject();
			go.GetComponent<Transform>().SetParent(trs);
		}
		return go.AddComponent<LineRenderer>();
	}

	public static void RemoveLineRendererAndGameObjectIfEmpty (LineRenderer lineRenderer)
	{
		Object destroyObject = lineRenderer;
		if (lineRenderer.GetComponents<Component>().Length == 2)
			destroyObject = lineRenderer.gameObject;
		GameManager.DestroyImmediate (lineRenderer);
	}

#if UNITY_EDITOR
	public static void RemoveLineRendererAndGameObjectIfEmpty (LineRenderer lineRenderer, bool destroyOnNextEditorUpdate)
	{
		if (!destroyOnNextEditorUpdate)
			RemoveLineRendererAndGameObjectIfEmpty (lineRenderer);
		else
		{
			Object destroyObject = lineRenderer;
			if (lineRenderer.GetComponents<Component>().Length == 2)
				destroyObject = lineRenderer.gameObject;
			GameManager.DestroyOnNextEditorUpdate (destroyObject);
		}
	}
#endif
}