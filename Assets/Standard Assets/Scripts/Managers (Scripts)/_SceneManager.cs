﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mirandus
{
	public class _SceneManager : SingletonMonoBehaviour<_SceneManager>, ISaveableAndLoadable
	{
		public static bool isLoading;
		public static Scene CurrentScene
		{
			get
			{
				return SceneManager.GetActiveScene();
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			isLoading = false;
		}
		
		public void LoadScene (string levelName)
		{
			isLoading = true;
			SceneManager.LoadScene(levelName);
		}
		
		public void LoadScene (int levelId)
		{
			isLoading = true;
			SceneManager.LoadScene(levelId);
		}
		
		public void LoadSceneAdditive (string levelName)
		{
			isLoading = true;
			SceneManager.LoadScene(levelName, LoadSceneMode.Additive);
		}
		
		public void LoadSceneAdditive (int levelId)
		{
			isLoading = true;
			SceneManager.LoadScene(levelId, LoadSceneMode.Additive);
		}
		
		public AsyncOperation LoadSceneAsyncAdditive (string levelName)
		{
			isLoading = true;
			return SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
		}
		
		public AsyncOperation LoadSceneAsyncAdditive (int levelId)
		{
			isLoading = true;
			return SceneManager.LoadSceneAsync(levelId, LoadSceneMode.Additive);
		}
		
		public void RestartScene ()
		{
			LoadScene (CurrentScene.name);
		}
		
		public void NextScene ()
		{
			LoadScene (CurrentScene.buildIndex + 1);
		}
	}
}