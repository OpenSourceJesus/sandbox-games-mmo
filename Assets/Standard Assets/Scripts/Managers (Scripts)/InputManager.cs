﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.XR.Oculus.Input;
using UnityEngine.InputSystem;
using Extensions;

namespace Mirandus
{
	public class InputManager : SingletonUpdateWhileEnabled<InputManager>
	{
		public InputDevice inputDevice;
		public static InputDevice _InputDevice
		{
			get
			{
				return Instance.inputDevice;
			}
		}
		public InputSettings settings;
		public static InputSettings Settings
		{
			get
			{
				return Instance.settings;
			}
		}
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				if (UsingGamepad)
					return false;
				else
					return Mouse.current.leftButton.isPressed;
			}
		}
		public bool _LeftClickInput
		{
			get
			{
				return LeftClickInput;
			}
		}
		public static Vector2 MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ReadValue();
				else
					return VectorExtensions.NULL3;
			}
		}
		public Vector2 _MousePosition
		{
			get
			{
				return MousePosition;
			}
		}
		public static Vector3 MoveInput
		{
			get
			{
				Vector3 output = Vector3.zero;
				if (Keyboard.current.wKey.isPressed)
					output = Quaternion.Inverse(GameCamera.instance.trs.rotation) * GameCamera.instance.trs.up;
				if (Keyboard.current.dKey.isPressed)
					output += Quaternion.Inverse(GameCamera.instance.trs.rotation) * GameCamera.instance.trs.right;
				if (Keyboard.current.aKey.isPressed)
					output += Quaternion.Inverse(GameCamera.instance.trs.rotation) * -GameCamera.instance.trs.right;
				if (Keyboard.current.sKey.isPressed)
					output += Quaternion.Inverse(GameCamera.instance.trs.rotation) * -GameCamera.instance.trs.up;
				return Vector3.ClampMagnitude(output, 1);
			}
		}
		public Vector3 _MoveInput
		{
			get
			{
				return MoveInput;
			}
		}
		public static bool JumpInput
		{
			get
			{
				if (_InputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.leftShiftKey.isPressed;
				else
					return false;
			}
		}
		public bool _JumpInput
		{
			get
			{
				return JumpInput;
			}
		}
		public static bool RestartInput
		{
			get
			{
				if (_InputDevice == InputDevice.KeyboardAndMouse)
					return Keyboard.current.rKey.isPressed;
				else
					return false;
			}
		}
		public bool _RestartInput
		{
			get
			{
				return RestartInput;
			}
		}
		public static bool SubmitInput
		{
			get
			{
				if (UsingGamepad)
					return Gamepad.current.aButton.isPressed;
				else
					return Keyboard.current.enterKey.isPressed;// || Mouse.current.leftButton.isPressed;
			}
		}
		public bool _SubmitInput
		{
			get
			{
				return SubmitInput;
			}
		}
		public static Vector2 UIMovementInput
		{
			get
			{
				if (UsingGamepad)
					return Vector2.ClampMagnitude(Gamepad.current.leftStick.ReadValue(), 1);
				else
				{
					int x = 0;
					if (Keyboard.current.dKey.isPressed)
						x ++;
					if (Keyboard.current.aKey.isPressed)
						x --;
					int y = 0;
					if (Keyboard.current.wKey.isPressed)
						y ++;
					if (Keyboard.current.sKey.isPressed)
						y --;
					return Vector2.ClampMagnitude(new Vector2(x, y), 1);
				}
			}
		}
		public Vector2 _UIMovementInput
		{
			get
			{
				return UIMovementInput;
			}
		}
		public static int? SwitchItemInput
		{
			get
			{
				if (Keyboard.current.digit1Key.isPressed)
					return 0;
				else if (Keyboard.current.digit2Key.isPressed)
					return 1;
				else if (Keyboard.current.digit3Key.isPressed)
					return 2;
				else if (Keyboard.current.digit4Key.isPressed)
					return 3;
				else if (Keyboard.current.digit5Key.isPressed)
					return 4;
				else if (Keyboard.current.digit6Key.isPressed)
					return 5;
				else if (Keyboard.current.digit7Key.isPressed)
					return 6;
				else if (Keyboard.current.digit8Key.isPressed)
					return 7;
				else if (Keyboard.current.digit9Key.isPressed)
					return 8;
				else if (Keyboard.current.digit0Key.isPressed)
					return 9;
				else
					return null;
			}
		}
		public int? _SwitchItemInput
		{
			get
			{
				return SwitchItemInput;
			}
		}
		public static bool UseItemInput
		{
			get
			{
				return LeftClickInput;
			}
		}
		public bool _UseItemInput
		{
			get
			{
				return UseItemInput;
			}
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse
		}
	}
}