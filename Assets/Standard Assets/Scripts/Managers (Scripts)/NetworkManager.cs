using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DarkRift.Client.Unity;
using DarkRift.Server.Unity;
using DarkRift.Client;
using DarkRift;
using System;
using TMPro;
using Extensions;

namespace Mirandus
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public UnityClient client;
		public XmlUnityServer server;
		public Player playerPrefab;
		public static Dictionary<ushort, Player> playersDict = new Dictionary<ushort, Player>();
		public static Dictionary<uint, SyncTransform> syncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static Dictionary<uint, SyncTransform> npcSyncTransformsDict = new Dictionary<uint, SyncTransform>();
		public static SpatialHashGrid2D<Entity> entitySpatialHashGrid;
		public static Dictionary<Entity, SpatialHashGrid2D<Entity>.Agent> entitySpatialHashGridAgentsDict = new Dictionary<Entity, SpatialHashGrid2D<Entity>.Agent>();
		public Rect entitySpatialHashGridRect;
		public float entitySpatialHashGridCellSize;
		public SpawnPoint[] spawnPoints = new SpawnPoint[0];
		// public TMP_Text playerListText;
		// public TMP_Text currentPlayerText;
		// public NotificationText notificationTextPrefab;
		// public Transform notificationTextsParent;
		
		void Start ()
		{
			SyncTransform[] syncTransforms = FindObjectsOfType<SyncTransform>();
			for (int i = 0; i < syncTransforms.Length; i ++)
			{
				SyncTransform syncTrs = syncTransforms[i];
				npcSyncTransformsDict.Add(syncTrs.id, syncTrs);
			}
			entitySpatialHashGrid = new SpatialHashGrid2D<Entity>(entitySpatialHashGridRect, entitySpatialHashGridCellSize);
		}
		
		public void Connect ()
		{
			client.MessageReceived -= OnMessageReceived;
			client.MessageReceived += OnMessageReceived;
			client.ConnectInBackground(client.Host, client.Port, false, OnConnectDone);
		}

		void OnConnectDone (Exception e)
		{
			if (e != null)
			{
				print(e.Message + "\n" + e.StackTrace);
				Connect ();
			}
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				if (message.Tag == MessageTags.SPAWN_PLAYER)
					OnSpawnPlayer (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_MOVED)
					OnSyncTransforMirandusved (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_ROTATED)
					OnSyncTransformRotated (sender, eventArgs);
				else if (message.Tag == MessageTags.PLAYER_LEFT)
					OnPlayerLeft (sender, eventArgs);
			}
		}

		void OnSpawnPlayer (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						ushort id = reader.ReadUInt16();
						Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						Quaternion rotation = Quaternion.Euler(Vector3.up * reader.ReadSingle());
						if (!playersDict.ContainsKey(id))
						{
							Player player = SpawnPlayer(position, rotation, id);
							// NotificationText notificationText = ObjectPool.instance.SpawnComponent<NotificationText>(notificationTextPrefab.prefabIndex, parent:notificationTextsParent);
							// notificationText.text.text = "Player " + id + " joined";
							player.syncTrs.idText.text = "" + id;
							playersDict.Add(id, player);
							syncTransformsDict.Add(id, player.syncTrs);
							// UpdatePlayerListText ();
							if (id == client.ID)
							// {
								player.enabled = true;
							// 	currentPlayerText.text = "You are: " + id;
							// }
						}
					}
				}
			}
		}

		void OnSyncTransforMirandusved (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Vector3 position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
						SyncTransform syncTrs;
						if (syncTransformsDict.TryGetValue(id, out syncTrs))
							syncTrs.trs.position = position;
					}
				}
			}
		}

		void OnSyncTransformRotated (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						float yRotation = reader.ReadSingle();
						SyncTransform syncTrs;
						if (syncTransformsDict.TryGetValue(id, out syncTrs))
							syncTrs.trs.eulerAngles = syncTrs.trs.eulerAngles.SetY(yRotation);
					}
				}
			}
		}

		void OnPlayerLeft (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						ushort id = reader.ReadUInt16();
						Player player;
						if (playersDict.TryGetValue(id, out player))
						{
							Destroy(player.gameObject);
							NetworkManager.entitySpatialHashGridAgentsDict.Remove(player);
							// NotificationText notificationText = ObjectPool.instance.SpawnComponent<NotificationText>(notificationTextPrefab.prefabIndex, parent:notificationTextsParent);
							// notificationText.text.text = "Player " + id + " left";
							playersDict.Remove(id);
							syncTransformsDict.Remove(id);
							// UpdatePlayerListText ();
						}
					}
				}
			}
		}

		public Player SpawnPlayer (Vector3 position, Quaternion rotation, ushort id)
		{
			Player output = ObjectPool.instance.SpawnComponent<Player>(playerPrefab.prefabIndex, position, rotation);
			output.ID = id;
			NetworkManager.entitySpatialHashGridAgentsDict.Add(output, new SpatialHashGrid2D<Entity>.Agent(position.GetXZ(), output, NetworkManager.entitySpatialHashGrid));
			return output;
		}

		// void UpdatePlayerListText ()
		// {
		// 	playerListText.text = "Players: ";
		// 	foreach (KeyValuePair<ushort, Player> keyValuePair in playersDict)
		// 		playerListText.text += keyValuePair.Key + ", ";
		// }

		public struct MessageTags
		{
			public const ushort SPAWN_PLAYER = 0;
			public const ushort SYNC_TRANSFORM_MOVED = 1;
			public const ushort SYNC_TRANSFORM_ROTATED = 2;
			public const ushort PLAYER_DIED = 3;
			public const ushort PLAYER_LEFT = 4;
			public const ushort PLAYER_STARTED_ACTION = 5;
		}
	}
}
