using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.IO;
using System.Globalization;

namespace Mirandus
{
	public class DRMManager : SingletonMonoBehaviour<DRMManager>
	{
		public int licenseEndYear;
		public int licenseEndMonth;
		public int licenseEndDay;
		public int licenseEndHour;
		public GameObject activateIfInvalidLicense;
		public float quitDelay;
		DateTime licenseEndTime;

		IEnumerator Start ()
		{
			licenseEndTime = new DateTime(licenseEndYear, licenseEndMonth, licenseEndDay, licenseEndHour, 0, 0, DateTimeKind.Utc);
			while (true)
			{
				bool gotCurrentTime = true;
				try
				{
					GameManager.currentTime = GetTime();
				}
				catch (Exception e)
				{
					gotCurrentTime = false;
				}
				if (gotCurrentTime)
				{
					if (NetworkManager.instance.client.enabled)
						NetworkManager.instance.Connect ();
					StartCoroutine(ValidateDRRoutine ());
					yield break;
				}
				yield return new WaitForEndOfFrame();
			}
			yield break;
		}

		IEnumerator ValidateDRRoutine ()
		{
			while (true)
			{
#if !UNITY_EDITOR
				if (GameManager.currentTime >= licenseEndTime)
				{
					EndGame ();
					yield break;
				}
#endif
				GameManager.currentTime = GameManager.currentTime.AddSeconds(Time.unscaledDeltaTime);
				yield return new WaitForEndOfFrame();
			}
		}

		public void EndGame ()
		{
			StartCoroutine(EndGameRoutine ());
		}

		IEnumerator EndGameRoutine ()
		{
			Time.timeScale = 0;
			activateIfInvalidLicense.SetActive(true);
			yield return new WaitForSecondsRealtime(quitDelay);
			Application.Quit();
		}

		static DateTime GetTime ()
		{
			HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create("http://www.microsoft.com");
			WebResponse response = webRequest.GetResponse();
			string dateText = response.Headers["date"];
			response.Dispose();
			return DateTime.ParseExact(dateText, 
										"ddd, dd MMM yyyy HH:mm:ss 'GMT'", 
										CultureInfo.InvariantCulture.DateTimeFormat, 
										DateTimeStyles.AssumeUniversal);
		}
	}
}