﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Mirandus
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISaveableAndLoadable
	{
		public GameObject[] registeredGos = new GameObject[0];
		public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuittingGame;
		public TileParent tileParentPrefab;
		[SaveAndLoadValue]
		public GameModifier[] gameModifiers = new GameModifier[0];
		public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		public static DateTime currentTime;

		public override void Awake ()
		{
			base.Awake ();
			if (instance != this)
				return;
			gameModifierDict.Clear();
			for (int i = 0; i < gameModifiers.Length; i ++)
			{
				GameModifier gameModifier = gameModifiers[i];
				gameModifierDict.Add(gameModifier.name, gameModifier);
			}
			Cursor.lockState = CursorLockMode.Locked;
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnDestroy ()
		{
			if (instance == this)
				SceneManager.sceneLoaded -= OnSceneLoaded;
		}
		
		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
			StopAllCoroutines();
			framesSinceLevelLoaded = 0;
		}

		void Update ()
		{
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			// if (Time.deltaTime > 0)
			// 	Physics.Simulate(Time.deltaTime);
			if (ObjectPool.Instance != null && ObjectPool.instance.enabled)
				ObjectPool.instance.DoUpdate ();
			InputSystem.Update ();
			// HandleRestart ();
			framesSinceLevelLoaded ++;
		}

		void HandleRestart ()
		{
			if (InputManager.RestartInput)
				_SceneManager.instance.RestartScene ();
		}

		public void ClearData ()
		{
			PlayerPrefs.DeleteAll();
			AccountManager.CurrentAccount.score = 0;
			_SceneManager.instance.LoadScene (0);
		}
		
		public void Quit ()
		{
			Application.Quit();
		}

		void OnApplicationQuit ()
		{
			// PlayerPrefs.DeleteAll();
			isQuittingGame = true;
		}

		public static void Log (object obj)
		{
			print(obj);
		}

		public static void DestroyImmediate (Object obj)
		{
			Object.DestroyImmediate(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
		
		public static void AddComponentOnNextUpdate<T> (GameObject go) where T : Component
		{
			EditorApplication.update += () => { AddComponent<T> (go); };
		}

		static void AddComponent<T> (GameObject go) where T : Component
		{
			EditorApplication.update -= () => { AddComponent<T> (go); };
			go.AddComponent<T>();
		}
#endif
		
		public static bool ModifierExistsAndIsActive (string name)
		{
			GameModifier gameModifier;
			if (gameModifierDict.TryGetValue(name, out gameModifier))
				return gameModifier.isActive;
			else
				return false;
		}

		public static bool ModifierIsActive (string name)
		{
			return gameModifierDict[name].isActive;
		}

		public static bool ModifierExists (string name)
		{
			return gameModifierDict.ContainsKey(name);
		}

		public static double GetTimeInSeconds ()
		{
			return GetTimeInSeconds(currentTime);
		}

		public static double GetTimeInSeconds (DateTime time)
		{
			return (double) time.Ticks / TimeSpan.TicksPerSecond;
		}

		[Serializable]
		public class GameModifier
		{
			public string name;
			public bool isActive;
		}
	}
}