using UnityEngine;

namespace Mirandus
{
	public interface ICollisionEnterHandler
	{
        Collider Collider { get; }
        
        void OnCollisionEnter (Collision coll);
	}
}