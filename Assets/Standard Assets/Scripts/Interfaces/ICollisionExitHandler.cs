using UnityEngine;

namespace Mirandus
{
	public interface ICollisionExitHandler
	{
        Collider Collider { get; }
        
        void OnCollisionExit (Collision coll);
	}
}