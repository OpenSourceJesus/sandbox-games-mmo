using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace Mirandus
{
	public class Tile : MonoBehaviour
	{
		public Transform trs;
		public MeshRenderer meshRenderer;
		public Tile[] neighbors = new Tile[0];
		public Tile[] supportingTiles = new Tile[0];
		public bool isSupportingTile;
		public TileParent tileParent;

		public virtual void OnEnable ()
		{
		}

		public virtual void OnDestroy ()
		{
			if (GameManager.paused || _SceneManager.isLoading || GameManager.isQuittingGame)
				return;
			for (int i = 0; i < neighbors.Length; i ++)
			{
				Tile tile = neighbors[i];
				tile.neighbors = tile.neighbors.Remove(this);
			}
			if (tileParent != null)
			{
				// tileParent.UpdateRigidbody ();
				tileParent.rigid.ResetCenterOfMass();
				tileParent.rigid.ResetInertiaTensor();
				tileParent.rigid.mass -= meshRenderer.bounds.GetVolume();
				ICollisionEnterHandler collisionEnterHandler = this as ICollisionEnterHandler;
				if (collisionEnterHandler != null)
					tileParent.collisionEnterHandlers = tileParent.collisionEnterHandlers.Remove(collisionEnterHandler);
			}
			for (int i = 0; i < neighbors.Length; i ++)
			{
				Tile neighbor = neighbors[i];
				Tile[] connectedTiles = Tile.GetConnectedGroup(neighbor);
				bool shouldSplit = true;
				for (int i2 = 0; i2 < neighbor.supportingTiles.Length; i2 ++)
				{
					Tile supportingTile = neighbor.supportingTiles[i2];
					if (connectedTiles.Contains(supportingTile) && supportingTile != null)
					{
						shouldSplit = false;
						break;
					}
				}
				if (shouldSplit)
					SplitTiles (connectedTiles);
			}
		}

		public static void SplitTiles (Tile[] tiles)
		{
			TileParent tileParent = Instantiate(GameManager.instance.tileParentPrefab);
			tileParent.name += TileParent.lastUniqueId;
			TileParent.lastUniqueId ++;
			tileParent.rigid.mass = 0;
			for (int i = 0; i < tiles.Length; i ++)
			{
				Tile tile = tiles[i];
				tile.trs.SetParent(tileParent.trs);
				if (tile.tileParent != null && tile.tileParent.trs.childCount == 0)
					Destroy(tile.tileParent.gameObject);
				tile.tileParent = tileParent;
				tileParent.rigid.mass += tile.meshRenderer.bounds.GetVolume();
			}
			// tileParent.UpdateRigidbody ();
			tileParent.rigid.ResetCenterOfMass();
			tileParent.rigid.ResetInertiaTensor();
			tileParent.collisionEnterHandlers = tileParent.GetComponentsInChildren<ICollisionEnterHandler>();
		}

		public static bool AreNeighbors (Tile tile, Tile tile2)
		{
			return tile.meshRenderer.bounds.Intersects(tile2.meshRenderer.bounds);
		}

		public static Tile[] GetConnectedGroup (Tile tile)
		{
			List<Tile> output = new List<Tile>();
			List<Tile> checkedTiles = new List<Tile>() { tile };
			List<Tile> remainingTiles = new List<Tile>() { tile };
			while (remainingTiles.Count > 0)
			{
				Tile tile2 = remainingTiles[0];
				output.Add(tile2);
				for (int i = 0; i < tile2.neighbors.Length; i ++)
				{
					Tile connectedTile = tile2.neighbors[i];
					if (!checkedTiles.Contains(connectedTile))
					{
						checkedTiles.Add(connectedTile);
						remainingTiles.Add(connectedTile);
					}
				}
				remainingTiles.RemoveAt(0);
			}
			return output.ToArray();
		}
	}
}