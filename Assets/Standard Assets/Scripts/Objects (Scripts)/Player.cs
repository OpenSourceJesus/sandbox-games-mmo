using UnityEngine;
using Extensions;
using System;
using System.Collections.Generic;

namespace Mirandus
{
	public class Player : Entity, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public ushort ID
		{
			get
			{
				return (ushort) syncTrs.id;
			}
			set
			{
				syncTrs.id = value;
			}
		}
		public GameObject cameraGo;
		public Item[] items = new Item[0];
		public UsableItem[] usableItems = new UsableItem[0];
		public Action[] actions = new Action[0];
		public Animator anim;
		Dictionary<string, Action> actionsDict = new Dictionary<string, Action>();
		UsableItem currentUsableItem;
		bool useItemInput;
		bool previousUseItemInput;

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instance = this;
			cameraGo.SetActive(true);
			currentUsableItem = usableItems[0];
			for (int i = 0; i < actions.Length; i ++)
			{
				Action action = actions[i];
				actionsDict.Add(action.name, action);
			}
			base.OnEnable ();
		}
		
		public override void DoUpdate ()
		{
			useItemInput = InputManager.UseItemInput;
			base.DoUpdate ();
			HandleItemSwitching ();
			HandleUseItem ();
			// foreach (KeyValuePair<uint, SyncTransform> keyValuePair in NetworkManager.syncTransformsDict)
			// 	keyValuePair.Value.canvasTrs.forward = GameCamera.instance.trs.position - keyValuePair.Value.canvasTrs.position;
			previousUseItemInput = useItemInput;
		}

		public override void HandleRotating ()
		{
			if (GameCamera.instance is ThirdPersonCamera)
				trs.forward = GameCamera.instance.trs.forward.GetXZ();
		}

		Vector3 GetMoveInput ()
		{
			Vector3 moveInput = InputManager.MoveInput;
			moveInput = moveInput.XYToXZ();
			moveInput = Quaternion.Euler(Vector3.up * trs.eulerAngles.y) * moveInput;
			return moveInput;
		}
		
		public override void HandleMoving ()
		{
			move = GetMoveInput();
			base.HandleMoving ();
		}
		
		public override void HandleJump ()
		{
			if (canJump && InputManager.JumpInput && Time.time - timeLastGrounded < maxJumpDuration)
			{
				if (isGrounded)
					yVel = 0;
				Jump ();
			}
			else
			{
				if (yVel > 0)
					yVel = 0;
				canJump = false;
			}
		}

		void HandleItemSwitching ()
		{
			if (InputManager.SwitchItemInput != null && usableItems.Length > (int) InputManager.SwitchItemInput)
				SwitchToItem (usableItems[(int) InputManager.SwitchItemInput]);
		}

		void SwitchToItem (UsableItem usableItem)
		{
			if (currentUsableItem == usableItem)
				return;
			if (currentUsableItem != null)
				currentUsableItem.Unequip ();
			usableItem.Equip ();
			currentUsableItem = usableItem;
		}

		void HandleUseItem ()
		{
			if (useItemInput && !previousUseItemInput)
				currentUsableItem.Use ();
		}

		public void DoAction (Action action, float startTime)
		{
			anim.Play(action.animStateName, action.animStateLayerIndex, ((float) GameManager.GetTimeInSeconds() - startTime) / action.duration);
		}

		[Serializable]
		public struct Action
		{
			public string name;
			public string animStateName;
			public int animStateLayerIndex;
			public float duration;
		}
	}
}