using UnityEngine;
using Extensions;
using Pathfinding;
using System.Collections.Generic;
// using System.Linq;

namespace Mirandus
{
	public class Enemy : Entity
	{
		public float targetDistanceFromPlayer;
		public Transform eyesTrs;
		public float visionRange;
		public float visionDegrees;
		public float visionSphereCastRadius;
		public LayerMask whatBlocksVision;
		public float minPlayerPositionChangeToRepath;
		float minPlayerPositionChangeToRepathSqr;
		float visionRangeSqr;
		Vector3 lastKnownPlayerPosition;
		float targetDistanceFromPlayerSqr;
		Player targetPlayer;
		bool canSeeTargetPlayer;

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (eyesTrs == null)
					eyesTrs = trs.Find("Eyes");
				return;
			}
#endif
			visionRangeSqr = visionRange * visionRange;
			targetDistanceFromPlayerSqr = targetDistanceFromPlayer * targetDistanceFromPlayer;
			minPlayerPositionChangeToRepathSqr = minPlayerPositionChangeToRepath * minPlayerPositionChangeToRepath;
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			NetworkManager.entitySpatialHashGridAgentsDict.Add(this, new SpatialHashGrid2D<Entity>.Agent(trs.position.GetXZ(), this, NetworkManager.entitySpatialHashGrid));
		}

		public override void DoUpdate ()
		{
			if (targetPlayer == null)
			{
				enabled = false;
				return;
			}
			HandleSetTargetPlayer ();
			canSeeTargetPlayer = CanSeePlayer(targetPlayer);
			HandlePathfinding ();
			base.DoUpdate ();
		}

		void HandleSetTargetPlayer ()
		{
			Player[] closebyPlayers = GetClosebyPlayers();
			for (int i = 0; i < closebyPlayers.Length; i ++)
			{
				Player player = closebyPlayers[i];
				float distanceSqr = (trs.position - player.trs.position).sqrMagnitude;
				if (distanceSqr <= visionRangeSqr && Vector3.Angle(eyesTrs.forward, player.trs.position - eyesTrs.position) <= visionDegrees / 2)
				{
					RaycastHit hit;
					if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, player.trs.position - eyesTrs.position, out hit, whatBlocksVision))
					{
						Player hitPlayer = hit.collider.GetComponent<Player>();
						if (hitPlayer != null)
						{
							SetTargetPlayerIfShould (hitPlayer);
							enabled = true;
						}
					}
				}
			}
		}

		public override void HandlePathfinding ()
		{
			if (canSeeTargetPlayer)
			{
				Vector3 playerPosition = targetPlayer.trs.position;
				if ((lastKnownPlayerPosition - playerPosition).sqrMagnitude >= minPlayerPositionChangeToRepathSqr)
				{
					SetDestination (playerPosition);
					lastKnownPlayerPosition = playerPosition;
				}
			}
			base.HandlePathfinding ();
		}
		
		public override void HandleRotating ()
		{
			if (canSeeTargetPlayer)
				trs.forward = (targetPlayer.trs.position - trs.position).GetXZ();
			else
				base.HandleRotating ();
		}
		
		public override void HandleMoving ()
		{
			if (canSeeTargetPlayer && (trs.position - targetPlayer.trs.position).sqrMagnitude < targetDistanceFromPlayerSqr)
				move = (trs.position - targetPlayer.trs.position).normalized;
			else
				base.HandleMoving ();
		}

		public override void Death ()
		{
			if (!isDead)
			{
				NetworkManager.npcSyncTransformsDict.Remove(syncTrs.id);
				NetworkManager.syncTransformsDict.Remove(syncTrs.id);
			}
			base.Death ();
		}

		bool SetTargetPlayerIfShould (Player player)
		{
			if (targetPlayer == null || (trs.position - targetPlayer.trs.position).sqrMagnitude > (trs.position - player.trs.position).sqrMagnitude)
			{
				targetPlayer = player;
				return true;
			}
			else
				return false;
		}

		bool CanSeePlayer (Player player)
		{
			if (Vector3.Angle(eyesTrs.forward, player.trs.position - eyesTrs.position) <= visionDegrees / 2)
			{
				RaycastHit hit;
				if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, player.trs.position - eyesTrs.position, out hit, whatBlocksVision))
				{
					if (hit.collider.GetComponent<Player>() == player)
						return true;
				}
			}
			return false;
		}
		
		Player[] GetClosebyPlayers ()
		{
			List<Player> output = new List<Player>();
			SpatialHashGrid2D<Entity>.Agent[] entitySpatialHashGridAgents = NetworkManager.entitySpatialHashGrid.GetClosebyAgents(trs.position.GetXZ());
			for (int i = 1; i < entitySpatialHashGridAgents.Length; i ++)
			{
				SpatialHashGrid2D<Entity>.Agent entitySpatialHashGridAgent = entitySpatialHashGridAgents[i];
				Player player = entitySpatialHashGridAgent.value as Player;
				if (player != null)
					output.Add(player);
			}
			return output.ToArray();
		}

		Player GetClosestPlayer ()
		{
			SpatialHashGrid2D<Entity>.Agent[] entitySpatialHashGridAgents = NetworkManager.entitySpatialHashGrid.GetClosebyAgents(trs.position.GetXZ());
			Player closestPlayer = null;
			float closestDistanceSqr = Mathf.Infinity;
			for (int i = 1; i < entitySpatialHashGridAgents.Length; i ++)
			{
				SpatialHashGrid2D<Entity>.Agent entitySpatialHashGridAgent = entitySpatialHashGridAgents[i];
				Player player = entitySpatialHashGridAgent.value as Player;
				if (player != null)
				{
					float distanceSqr = (trs.position - player.trs.position).sqrMagnitude;
					if (distanceSqr < closestDistanceSqr)
					{
						closestPlayer = player;
						closestDistanceSqr = distanceSqr;
					}
				}
			}
			return closestPlayer;
		}
	}
}