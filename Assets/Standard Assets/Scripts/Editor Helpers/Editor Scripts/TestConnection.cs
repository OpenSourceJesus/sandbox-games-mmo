#if UNITY_EDITOR
using System.Collections;
using UnityEngine;

namespace Mirandus
{
	public class TestConnection : EditorScript
	{
		public string[] tryHosts = new string[0];
		public ushort[] tryPorts = new ushort[0];
		
		public override void Do ()
		{
			StartCoroutine(DoRoutine ());
		}

		IEnumerator DoRoutine ()
		{
			for (int i = 0; i < tryHosts.Length; i ++)
			{
				for (int i2 = 0; i2 < tryPorts.Length; i2 ++)
				{
					NetworkManager.Instance.client.Host = tryHosts[i];
					NetworkManager.instance.client.Port = tryPorts[i2];
					NetworkManager.instance.Connect ();
					// yield return new WaitForSeconds(3);
					yield return new WaitForEndOfFrame();
				}
			}
		}
	}
}
#else
namespace Mirandus
{
	public class TestConnection : EditorScript
	{
	}
}
#endif