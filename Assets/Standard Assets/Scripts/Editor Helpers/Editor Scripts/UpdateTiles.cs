#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

namespace Mirandus
{
	public class UpdateTiles : EditorScript
	{
		[MenuItem("Tools/Update Tiles")]
		static void _UpdateTiles ()
		{
			Tile[] tiles = SelectionExtensions.GetSelected<Tile>();
			for (int i = 0; i < tiles.Length; i ++)
			{
				Tile tile = tiles[i];
				if (!tile.enabled)
				{
					tiles = tiles.RemoveAt(i);
					i --;
				}
				else
					tile.neighbors = new Tile[0];
			}
			for (int i = 0; i < tiles.Length; i ++)
			{
				Tile tile = tiles[i];
				for (int i2 = 0; i2 < tiles.Length; i2 ++)
				{
					Tile tile2 = tiles[i2];
					if (i != i2 && Tile.AreNeighbors(tile, tile2))
						tile.neighbors = tile.neighbors.Add(tile2);
				}
			}
			List<Tile> _tiles = new List<Tile>(tiles);
			List<Tile[]> connectedTileGroups = new List<Tile[]>();
			List<Tile[]> supportingTileGroups = new List<Tile[]>();
			List<Tile> remainingTiles = new List<Tile>(tiles);
			while (remainingTiles.Count > 0)
			{
				Tile[] connectedTileGroup = Tile.GetConnectedGroup(remainingTiles[0]);
				connectedTileGroups.Add(connectedTileGroup);
				List<Tile> supportingTiles = new List<Tile>();
				for (int i = 0; i < connectedTileGroup.Length; i ++)
				{
					Tile connectedTile = connectedTileGroup[i];
					if (connectedTile.isSupportingTile)
						supportingTiles.Add(connectedTile);
				}
				for (int i = 0; i < connectedTileGroup.Length; i ++)
				{
					Tile connectedTile = connectedTileGroup[i];
					remainingTiles.Remove(connectedTile);
				}
				supportingTileGroups.Add(supportingTiles.ToArray());
			}
			for (int i = 0; i < connectedTileGroups.Count; i ++)
			{
				Tile[] connectedTileGroup = connectedTileGroups[i];
				Tile[] supportingTileGroup = supportingTileGroups[i];
				for (int i2 = 0; i2 < connectedTileGroup.Length; i2 ++)
				{
					Tile tile = connectedTileGroup[i2];
					tile.supportingTiles = supportingTileGroup;
					tile.enabled = !tile.enabled;
					tile.enabled = !tile.enabled;
				}
			}
		}
	}
}
#endif
