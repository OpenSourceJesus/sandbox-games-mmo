#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEditor;

namespace Mirandus
{
	[ExecuteInEditMode]
	public class ChangeTileSize : EditorScript
	{
		public Tile tile;
		public int addToRightSide;
		public int addToLeftSide;
		public int addToUpSide;
		public int addToDownSide;
		public int addToFrontSide;
		public int addToBackSide;

		public override void Do ()
		{
			if (tile == null)
				tile = GetComponent<Tile>();
			_Do (tile, addToRightSide, addToLeftSide, addToUpSide, addToDownSide, addToFrontSide, addToBackSide);
		}

		public static void _Do (Tile tile, int addToRightSide = 0, int addToLeftSide = 0, int addToUpSide = 0, int addToDownSide = 0, int addToFrontSide = 0, int addToBackSide = 0)
		{
			Bounds bounds = tile.meshRenderer.bounds;
			Vector3 sizeChange = new Vector3(addToRightSide - addToLeftSide, addToUpSide - addToDownSide, addToFrontSide - addToBackSide);
			bounds.center += sizeChange / 2;
			bounds.size += sizeChange;
			Transform[] transforms = MakeGridOfTransformPrefabsInBounds._Do (PrefabUtility.GetCorrespondingObjectFromSource(tile.trs), bounds, Vector3.one, new BoundsOffset(Vector3.one / 2, Vector3.zero), tile.trs.parent);
			ProBuilderMesh[] proBuilderMeshes = new ProBuilderMesh[transforms.Length];
			for (int i = 0; i < transforms.Length; i ++)
			{
				Transform trs = transforms[i];
				proBuilderMeshes[i] = trs.GetComponent<ProBuilderMesh>();
			}
			proBuilderMeshes = MergeProBuilderMeshes._Do (proBuilderMeshes, 2);
			for (int i = 0; i < proBuilderMeshes.Length; i ++)
			{
				ProBuilderMesh proBuilderMesh = proBuilderMeshes[i];
				EditorApplication.update += () => { ResetBoxCollider (proBuilderMesh.gameObject); };
				DeleteProBuilderMeshSharedFaces._Do (proBuilderMesh);
			}
			GameManager.DestroyOnNextEditorUpdate (tile.gameObject);
		}

		static void ResetBoxCollider (GameObject go)
		{
			EditorApplication.update -= () => { ResetBoxCollider (go); };
			DestroyImmediate(go.GetComponent<BoxCollider>());
			go.AddComponent<BoxCollider>();
		}
	}
}
#else
namespace Mirandus
{
	public class ChangeTileSize : EditorScript
	{
	}
}
#endif