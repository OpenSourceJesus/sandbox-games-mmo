using UnityEngine;
using UnityEngine.ProBuilder.MeshOperations;
using UnityEngine.ProBuilder;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Extensions;

namespace Mirandus
{
	public class MergeProBuilderMeshes : EditorScript
	{
		public ProBuilderMesh[] proBuilderMeshes = new ProBuilderMesh[0];
		public int maxBatchSize = 2;

		public override void Do ()
		{
			_Do (proBuilderMeshes, maxBatchSize);
			proBuilderMeshes = new ProBuilderMesh[0];
		}

		public static ProBuilderMesh[] _Do (ProBuilderMesh[] proBuilderMeshes, int maxBatchSize)
		{
			List<ProBuilderMesh> mergedProBuilderMeshes = new List<ProBuilderMesh>();
			List<ProBuilderMesh> _proBuilderMeshes = new List<ProBuilderMesh>(proBuilderMeshes);
			int previousMergeCount = _proBuilderMeshes.Count;
			while (true)
			{
				int batchSize = Mathf.Min(maxBatchSize, _proBuilderMeshes.Count);
				if (batchSize < 2)
					break;
				ProBuilderMesh[] mergeProBuilderMeshes = new ProBuilderMesh[batchSize];
				for (int i2 = 0; i2 < batchSize; i2 ++)
				{
					mergeProBuilderMeshes[i2] = _proBuilderMeshes[0];
					_proBuilderMeshes.RemoveAt(0);
				}
				mergedProBuilderMeshes = CombineMeshes.Combine(mergeProBuilderMeshes, mergeProBuilderMeshes[0]);
				_proBuilderMeshes.AddRange(mergedProBuilderMeshes);
				for (int i2 = 1; i2 < mergeProBuilderMeshes.Length; i2 ++)
				{
					ProBuilderMesh proBuilderMesh = mergeProBuilderMeshes[i2];
					if (!mergedProBuilderMeshes.Contains(proBuilderMesh))
#if UNITY_EDITOR
						GameManager.DestroyOnNextEditorUpdate (proBuilderMesh.gameObject);
#else
						GameManager.DestroyImmediate (proBuilderMesh.gameObject);
#endif
				}
				if (previousMergeCount == _proBuilderMeshes.Count)
					break;
				previousMergeCount = _proBuilderMeshes.Count;
			}
			return mergedProBuilderMeshes.ToArray();
		}

#if UNITY_EDITOR
		[MenuItem("Tools/Merge selected ProBuilderMeshes")]
		static void _Do ()
		{
			_Do (SelectionExtensions.GetSelected<ProBuilderMesh>(), int.MaxValue);
		}
#endif
	}
}