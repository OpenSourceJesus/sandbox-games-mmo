using System.Collections;
using System.Collections.Generic;
using DarkRift.Server;
using DarkRift;
using System;
using UnityEngine;
using Random = UnityEngine.Random;
using MessageTags = Mirandus.NetworkManager.MessageTags;

namespace Mirandus
{
	public class MirandusPlugin : Plugin
	{
		public override Version Version => new Version(1, 0, 0);
		public override bool ThreadSafe => true;
		public static MirandusPlugin instance;

		public MirandusPlugin (PluginLoadData pluginLoadData) : base (pluginLoadData)
		{
			instance = this;
			ClientManager.ClientConnected += OnClientConnected;
			ClientManager.ClientDisconnected += OnClientDisconnected;
		}

		void OnClientConnected (object sender, ClientConnectedEventArgs eventArgs)
		{
			byte spawnPointIndex = (byte) Random.Range(0, NetworkManager.instance.spawnPoints.Length);
			SpawnPoint spawnPoint = NetworkManager.instance.spawnPoints[spawnPointIndex];
			Player player = ObjectPool.instance.SpawnComponent<Player>(NetworkManager.instance.playerPrefab.prefabIndex, spawnPoint.trs.position, spawnPoint.trs.rotation);
			player.ID = eventArgs.Client.ID;
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(player.ID);
				writer.Write(spawnPoint.trs.position.x);
				writer.Write(spawnPoint.trs.position.y);
				writer.Write(spawnPoint.trs.position.z);
				writer.Write(spawnPoint.trs.eulerAngles.y);
				using (Message message = Message.Create(MessageTags.SPAWN_PLAYER, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						// if (client != eventArgs.Client)
							client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				foreach (Player previousPlayer in NetworkManager.playersDict.Values)
				{
					writer.Write(previousPlayer.ID);
					writer.Write(previousPlayer.trs.position.x);
					writer.Write(previousPlayer.trs.position.y);
					writer.Write(previousPlayer.trs.position.z);
					writer.Write(previousPlayer.trs.eulerAngles.y);
					using (Message message = Message.Create(MessageTags.SPAWN_PLAYER, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			foreach (SyncTransform npcSyncTrs in NetworkManager.npcSyncTransformsDict.Values)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(npcSyncTrs.id);
					writer.Write(npcSyncTrs.trs.position.x);
					writer.Write(npcSyncTrs.trs.position.y);
					writer.Write(npcSyncTrs.trs.position.z);
					using (Message message = Message.Create(MessageTags.SYNC_TRANSFORM_MOVED, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(npcSyncTrs.id);
					writer.Write(npcSyncTrs.trs.eulerAngles.y);
					using (Message message = Message.Create(MessageTags.SYNC_TRANSFORM_ROTATED, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			NetworkManager.playersDict.Add(player.ID, player);
			NetworkManager.syncTransformsDict.Add(player.ID, player.syncTrs);
			eventArgs.Client.MessageReceived += OnMessageReceived;
		}

		void OnClientDisconnected (object sender, ClientDisconnectedEventArgs eventArgs)
		{
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(eventArgs.Client.ID);
				using (Message message = Message.Create(MessageTags.PLAYER_LEFT, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			NetworkManager.playersDict.Remove(eventArgs.Client.ID);
			NetworkManager.syncTransformsDict.Remove(eventArgs.Client.ID);
			eventArgs.Client.MessageReceived -= OnMessageReceived;
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				if (message.Tag == MessageTags.SYNC_TRANSFORM_MOVED)
					OnSyncTransformMoved (sender, eventArgs);
				else if (message.Tag == MessageTags.SYNC_TRANSFORM_ROTATED)
					OnSyncTransformRotated (sender, eventArgs);
			}
		}

		void OnSyncTransformMoved (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					SyncTransform syncTrs = NetworkManager.syncTransformsDict[reader.ReadUInt32()];
					syncTrs.trs.position = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, eventArgs.SendMode);
					}
				}
			}
		}

		void OnSyncTransformRotated (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					SyncTransform syncTrs = NetworkManager.syncTransformsDict[reader.ReadUInt32()];
					syncTrs.trs.eulerAngles = Vector3.up * reader.ReadSingle();
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, eventArgs.SendMode);
					}
				}
			}
		}
	}
}