# Mirandus
Builds not included  
Tested on Windows and Linux and should work on Mac too  
Code is found in Assets/Standard Assets/Scripts  
The backend (server-side) code has been compiled from the code files found under Assets/Standard Assets/DarkRift/DarkRift/Plugins/Mirandus  
Code files to be aware of:  
- Managers (Scripts)/GameManager.cs  
- Managers (Scripts)/InputManager.cs  
- Managers (Scripts)/NetworkManager.cs  
- Concepts/MonoBehaviours/Update While Enableds/Entity.cs  
- Concepts/MonoBehaviours/Update While Enableds/Singleton Update While Enableds/Camera Scripts/GameCamera.cs  
- Objects (Scripts)/Player.cs  
- Objects (Scripts)/Enemy.cs  

There are some code files that are not used

## How to play
The enemies (red capsules) will only follow you once they see you  
The enemies' orientations are not synced  
The enemies try to keep a certain distance from the player

### Controls
WASD keys to move  
Move mouse to look  
Hold Left-Shift to jump
